### NAME

randombytes - fill a buffer with random data

### SYNOPSIS

    #include <randombytes.h>

    unsigned char x[xbytes];

    randombytes(x,xbytes);

Link with `-lrandombytes`.

### DESCRIPTION

`randombytes` sets `x[0]`, `x[1]`, ..., `x[xbytes-1]` to random bytes of
data.

Randomness APIs vary in three major ways. `randombytes` is designed in
each way to simplify callers:

* Like `RAND_bytes`, `randombytes` automatically generates separate
  randomness for any number of bytes in any number of calls in any
  number of threads in any number of programs. For comparison, some
  randomness APIs (e.g., `random`) recycle randomness from one program
  to another unless the program does extra work to set a separate
  "seed", and can recycle randomness across multiple threads unless the
  program does further work.

* Like `getrandom` and `getentropy` and `RAND_bytes`, `randombytes` aims
  for the stringent goal of ensuring that no feasible computation will
  ever be able to tell the difference between the output bytes and true
  randomness. The caller can treat each returned byte as the result of 8
  fresh coin flips. For comparison, some randomness APIs (e.g.,
  `random`) do not aim for this goal and do not view detectable patterns
  as a bug, as long as _most_ applications do not notice the patterns.

* Like `random`, `randombytes` always succeeds. Any necessary resources
  (e.g., opening a file descriptor for `/dev/urandom`, on systems that
  need this) are allocated at program startup, rather than being
  deferred until the first `randombytes` call; also, dynamic failure
  cases such as EINTR are handled inside `randombytes`. For comparison,
  some randomness APIs (e.g., `getrandom` and `getentropy` and
  `RAND_bytes`) can return error codes to be handled by the caller. 

There are some programs that try to close all file descriptors. These
programs must limit their library use to libraries that promise not to
keep file descriptors open. In particular, these programs must not use
`randombytes` (which keeps a file descriptor open on some systems) or
other libraries calling `randombytes`.

### LINK DETAILS

Currently `-lrandombytes` is a frontend symbolic link to either
`-lrandombytes-kernel` or `-lrandombytes-openssl` as a backend library.
To simplify system-wide replacement of the backend library, typical
applications should dynamically link to `-lrandombytes` rather than to
`-lrandombytes-kernel` or `-lrandombytes-openssl`.

Applications that link statically to `-lrandombytes` also need
`-lcrypto` if `-lrandombytes` is `-lrandombytes-openssl`.

Currently `randombytes` is a macro, where the function actually linked
is `randombytes_internal_void_voidstar_longlong`.

### HISTORY

The `randombytes` API was introduced in 2008 as part of the
[SUPERCOP](https://bench.cr.yp.to)
benchmarking framework for cryptographic software.

Similar previous APIs include `RAND_bytes` and `arc4random_buf`, but
`RAND_bytes` was allowed to return failures and `arc4random_buf` was
using the broken RC4 stream cipher.

### SEE ALSO

**getrandom**(2), **getentropy**(2), **rand**(3), **random**(3),
**arc4random**(3), **urandom**(4)
