To download and unpack the latest version of librandombytes:

    wget -m https://randombytes.cr.yp.to/librandombytes-latest-version.txt
    version=$(cat randombytes.cr.yp.to/librandombytes-latest-version.txt)
    wget -m https://randombytes.cr.yp.to/librandombytes-$version.tar.gz
    tar -xzf randombytes.cr.yp.to/librandombytes-$version.tar.gz
    cd librandombytes-$version

Then [install](install.html).

### Archives and changelog (reverse chronological)

[`librandombytes-20240318.tar.gz`](librandombytes-20240318.tar.gz) [browse](librandombytes-20240318.html)

More work on port to MacOS X, handling differences in shared-library naming.

Add `randombytes-info` manual page.

[`librandombytes-20230919.tar.gz`](librandombytes-20230919.tar.gz) [browse](librandombytes-20230919.html)

Port to MacOS X, fixing use of `getentropy`.
Thanks to Jan Mojzis.

[`librandombytes-20230905.tar.gz`](librandombytes-20230905.tar.gz) [browse](librandombytes-20230905.html)

Split [`license.md`](license.html) out of [`readme.md`](index.html),
and add further SPDX options.

Tweak CSS for better legibility.

Copy various `./configure` improvements from libmceliece and lib25519.

[`librandombytes-20230126.tar.gz`](librandombytes-20230126.tar.gz) [browse](librandombytes-20230126.html)
