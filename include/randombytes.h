// version 20230126
// public domain
// djb

// public API: randombytes()

#ifndef randombytes_h
#define randombytes_h

#define randombytes randombytes_internal_void_voidstar_longlong
#define randombytes_source randombytes_internal_source

#ifdef __cplusplus
extern "C" {
#endif

extern void randombytes(void *,long long) __attribute__((visibility("default")));
extern const char *randombytes_source(void) __attribute__((visibility("default")));

#ifdef __cplusplus
}
#endif

#endif
