#ifndef getentropy_wrapper_h
#define getentropy_wrapper_h

#define getentropy_wrapper_ready randombytes_internal_getentropy_wrapper_ready
#define getentropy_wrapper randombytes_internal_getentropy_wrapper

#ifdef __cplusplus
extern "C" {
#endif

extern int getentropy_wrapper_ready(void);
extern void getentropy_wrapper(void *,long long);

#ifdef __cplusplus
}
#endif

#endif
